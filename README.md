### Usage

#### chart from 'order_book' table

See [test/test_lob.R](./test/test_lob.R)

Will produce something like

![example1](./lob-ex1.png)

#### chart from 'taq' table

See [test/test_chart.R](./test/test_chart.R)

![example](./ex-chart.png)

### chart own trades (unimm_utnp)
See [test/test_mytrades.R](./test/test_mytrades.R)

![example](./ex-mytrades.png)


### chart balances
See [test/test_balances.R](./test/test_balances.R)

![example](./ex-balances.png)

#### apps

See [https://wiki.lulzex.com/u/mike/LOB](https://wiki.lulzex.com/u/mike/LOB)

